# Teste de CI/CD em Jenkins
Este teste foi criado com exclusividade para a [Rivendel](http://rivendel.com.br/). Com o objetivo de testar os conhecimentos dos candidatos ao cargo de DevOps, permitindo que seja implementado os conhecimentos de metodologias ágeis e ciclos no desenvolvimento de software.

## Getting Started

As seguinte instruções irão auxiliar no setup do ambiente, para que seja possível iniciar o desenvolvimento do pipeline em um sandbox.

O primeiro passo é criar um fork deste projeto para a sua própria conta, permitingo o livre desenvolvimento do pipeline. Assim como facilitará a avaliação por parte da Rivendel.

Assim que o fork estiver criado, cadastre a [chave pública deste teste](rivendel.id_rsa.pub) nas `Access Keys` do repositório, para que a Rivendel consiga clonar e testar ele.

Você também deve setar o ID: `e48b2566-1fe7-4405-ab10-de3fbc62fd88` da credential para que possamos clonar seu repositório.

Esperamos que seu pipeline aceite receber uma variavel por parametro de nome `branchName`.

Caso utilize Git Plugin, segue exemplo:
```
    git branch: env.branchName,
        credentialsId: "e48b2566-1fe7-4405-ab10-de3fbc62fd88",
        url: "<url do seu repositório>"
```

### Prerequisites

Para conseguir executar o teste você precisará dos seguintes softwares instalados:

 - [Jenkins](https://github.com/jenkinsci/jenkins)
 - [Docker](https://www.docker.com/what-docker)

Ambos podem ser instalados em algum ambiente para desenvolvimento, ou pode ser utilizado o [Vagrant](https://www.vagrantup.com/intro/index.html) para criar a VM com o [Vagrantfile](Vagrantfile). Executando apenas:

```shell
# Creating development env
vagrant up
```

#### Credenciais de acesso ao Jenkins do Vagrant
- ***[Acesso ao Jenkins do Vagrant](http://192.168.33.10:8080)***
- ***Usuário:*** `admin`
- ***Senha:*** `jenkins`

## Objetivo do teste

Implementar um pipeline para Continuous Integration + Continuous Deployment do microserviço que acompanha o repositório, e foi criado em Python 3.6. A imagem necessária para executar esse microserviço está especificada em [Docker](Dockerfile).

Espera-se que o código do pipeline seja implementado no [Jenkinsfile](Jenkinsfile).

### Deployment

Um container deve ser criado na mesma instância do Jenkins por meio do pipeline, com a API respondendo na porta 80.

Boas práticas de desenvolvimento de software serão consideradas. Assim como o fluxo end-to-end do Deployment.

## Built With

* [Jenkins CI](https://github.com/jenkinsci/jenkins)
* [Docker Engine](https://www.docker.com/what-docker)
* [Python 3.6](https://www.python.org/about/)
* [Connexion](https://connexion.readthedocs.io/en/latest/)
* [OpenAPI Specification](https://github.com/OAI/OpenAPI-Specification)

## Finalizando

Assim que tiver finalizado a criação do pipeline, responda nosso email com o link ssh de seu repositório, então iremos executar o `Jenkinsfile` e avaliar o que foi produzido.

[![Rivendel Tecnologia](http://res.cloudinary.com/hrscywv4p/image/upload/c_limit,fl_lossy,h_1440,w_720,f_auto,q_auto/v1/782436/Screen_Shot_2016-03-05_at_17.45.10_c9l4og.png)](http://www.rivendel.com.br)
