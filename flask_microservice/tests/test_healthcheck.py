import os
import sys
import unittest

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

from flask_microservice import app
from flask_microservice.tests import test_app

class TestHealthCheck(unittest.TestCase):

    def test_healthcheck(self):
        resp = test_app.get('/healthcheck')
        self.assertEqual(200, resp.status_code)
