import os
import sys
import json
import unittest

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

from flask_microservice import app
from flask_microservice.tests import test_app

class TestHelloApi(unittest.TestCase):

    def test_hello_get(self):
        resp = test_app.get('/helloworld')
        self.assertEqual("Hello, World!", resp.data.decode('utf-8'))
        self.assertEqual(200, resp.status_code)

    def test_hello_post(self):
        resp = test_app.post('/helloworld',
            data=json.dumps(
                dict(name='Gandalf')
            ),
            content_type='application/json'
        )
        self.assertEqual('"Hello Gandalf"\n', resp.data.decode('utf-8'))
        self.assertEqual(200, resp.status_code)
